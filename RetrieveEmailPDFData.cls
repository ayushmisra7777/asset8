public with sharing class x7S_RetrieveEmailPDFData {
public static Id caserecID;
public static List<Case> caseList{get;set;}
public static List<ARC_Case_Services__c> serviceList{get;set;}
public static String languageSelect{get;set;}



public x7S_RetrieveEmailPDFData(){
    caserecID  = ApexPages.CurrentPage().getparameters().get('id');            
    caseList=[SELECT Id,ContactId, CaseNumber,Event__r.Name,ClosedDate,CreatedDate,Number_of_Clean_Up_Kits__c,Number_of_Comfort_Kits__c,
                Number_of_Blankets__c,Number_of_Toys__c,Number_of_Bottled_Water__c,Number_of_Snacks__c
                FROM Case
                WHERE Id=:caserecID AND Event__c!=null limit 1];
    
    serviceList=[SELECT id,Name,Entry_Date__c,case__c,Payment_Type__c,Gift_Amount__c,VSP_Number__c,
                    Agency_Name__c,Agency_Address__c,Agency_Website__c,Phone__c,Required_Documentation__c,Last_Status_Change__c
                    FROM ARC_Case_Services__c WHERE case__c=:caserecID];
    List<Contact> client=[SELECT Id,Name,Email,Language_1__c From Contact WHERE Id=:caseList[0].ContactId LIMIT 1];
    languageSelect= 'en_US';
    if(!client.isEmpty() && client[0].Language_1__c!=null){
        
        if(client[0].Language_1__c == 'English'){
            languageSelect= 'en_US';
        }
        if(client[0].Language_1__c == 'Spanish'){
            languageSelect= 'es';
        }
    }
}


@AuraEnabled
@future(callout=true)
public static void savePdf( Id caseId){        
    caserecID = caseId;
    caseList=[SELECT Id,ContactId, CaseNumber,Event__r.Name,ClosedDate,CreatedDate,Number_of_Clean_Up_Kits__c,Number_of_Comfort_Kits__c,
                Number_of_Blankets__c,Number_of_Toys__c,Number_of_Bottled_Water__c,Number_of_Snacks__c
                FROM Case
                WHERE Id=:caserecID AND Event__c!=null limit 1];
    
    serviceList=[SELECT id,Name,Entry_Date__c,case__c,Payment_Type__c,Gift_Amount__c,VSP_Number__c,Agency_Name__c,Agency_Address__c,Agency_Website__c,Phone__c,Required_Documentation__c,Last_Status_Change__c
                    FROM ARC_Case_Services__c WHERE case__c=:caserecID];
    
    if(!caseList.isEmpty())
    {
        String pdfName='Case Summary';
        PageReference pdf = Page.caseCloseSummaryPDF;
        pdf.getParameters().put('id',caseList[0].Id);
        Blob body;
        try {   
            body = pdf.getContent();
            System.debug('!@#$%'+ body);   
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
        System.debug('!@#$%'+ body);
        ContentVersion conVer = new ContentVersion();
        conVer.Origin = 'H';
        conVer.ContentLocation = 'S';        
        conVer.PathOnClient = 'Captured.pdf'; 
        conVer.Title = pdfName;             
        conVer.VersionData = body; 
        insert conVer;
        system.debug('conVer :'+conVer.id);
        
        List<ContentVersion> getContentvE= [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id];
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        system.debug('getContentvE :'+getContentvE[0].ContentDocumentId);
        system.debug('conDoc :'+conDoc);
        
        ContentDocumentLink conDocLink = New ContentDocumentLink();
        conDocLink.LinkedEntityId = caseList[0].Id; 
        conDocLink.ContentDocumentId = conDoc;  
        conDocLink.shareType = 'V';
        conDocLink.Visibility ='AllUsers';
        insert conDocLink;
        
        List<Contact> client=[SELECT Id,Name,Email,Account.Languages_Spoken_in_Household__c ,Language_1__c From Contact WHERE Id=:caseList[0].ContactId LIMIT 1];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: x7S_RCCareHelper.ORG_EMAIL]; // org wide email code added by Hari for ncm-2959

        EmailTemplate emailTemplate = new EmailTemplate();
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        languageSelect= 'en_US';
        if(!client.isEmpty() && client[0].Language_1__c!=null){
        
            if(client[0].Language_1__c == 'English'){
            languageSelect= 'en_US';
            emailTemplate = [SELECT Id, Name,Subject,Description,HtmlValue,DeveloperName,Body 
                                FROM EmailTemplate Where Name= 'Summary Of Assistance Template – To Client (Case Closed)_English'];
            }
            if(client[0].Language_1__c == 'Spanish'){
            languageSelect= 'es';
            emailTemplate = [SELECT Id, Name,Subject,Description,HtmlValue,DeveloperName,Body 
                                FROM EmailTemplate Where Name= 'Summary Of Assistance Template – To Client (Case Closed)_Spanish'];
            }
         }
        System.debug('languageSelect'+languageSelect);

        email.setTemplateID(emailTemplate.Id); 
        email.setTargetObjectId(client[0].Id);
        // Create the email attachment    
        efa.setFileName('Case Summary.pdf');
        efa.setBody(body);
        // Set the paramaters to the email object
        // org wide email code added by Hari for ncm-2959
        if ( owea.size() > 0 ) {
        email.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        // org wide email code added by Hari for ncm-2959
        //  email.setSubject( 'Summary Of Assistance Template – To Client (Case Closed)' );
        
        // Here I am accessing current user email id to whom we are sending email
        email.setToAddresses( new String[] {client[0].Email} );

        // Attach file using setFileAttachments function
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        // Sends the email
        Messaging.SendEmailResult [] r = 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
}
}